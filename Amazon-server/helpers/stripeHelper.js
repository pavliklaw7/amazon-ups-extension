import {createRequire} from 'module'

const require = createRequire(import.meta.url)

const stripe = require('stripe')('sk_test_51IqL0AJjFNxo32NL9dWUlhF3FMdi13RfFP1maVlVS73pnjCtTOekI3Ox86KN5qK1eGERGhGKCZLVsoK2jXM1Kl1t00cPlqmBrl')

export class StripeHelper {
    constructor() {
    }

    async createCustomer(email) {
        try {
            const customers = await this.listCustomers()

            if (customers.length !== 0) {
                let found = customers.find(value => value.email === email)
                if (found != null) {
                    return found
                }
            }

            return await stripe.customers.create({
                email
            })
        } catch (ex) {
            throw ex
        }

    }

    async listCustomers(email) {
        try {
            const cs = []

            for await (const c of stripe.customers.list({email})) {
                cs.push(c)
            }

            return cs
        } catch (ex) {
            throw ex
        }
    }

    async listSubs(customerId) {
        try {
            const ss = []

            for await (const s of stripe.subscriptions.list({customer: customerId})) {
                ss.push(s)
            }

            return ss
        } catch (ex) {
            throw ex
        }
    }

    async getIdByEmail(email) {
        try {
            return (await this.listCustomers(email))[0].id
        } catch (ex) {
            throw ex
        }
    }

    async listProducts() {
        try {
            const ps = []

            for await (const p of stripe.products.list()) {
                ps.push(p)
            }

            return ps
        } catch (ex) {
            throw ex
        }
    }

    async listProductsWithName(name) {
        try {
            const ps = await this.listProducts()

            if (!!name) {
                const r = []

                for (const p of ps) {
                    if (p.name === name) {
                        r.push(p)
                    }
                }

                return r
            }

            return ps
        } catch (ex) {
            throw ex
        }
    }

    async createCheckoutSession(priceId, customerId) {
        try {
            return await stripe.checkout.sessions.create({
                mode: 'subscription',
                payment_method_types: ['card'],
                line_items: [
                    {
                        price: priceId,
                        quantity: 1
                    }
                ],
                customer: customerId,
                success_url: 'https://example.com/success',
                cancel_url: 'https://example.com/cancel'
            })
        } catch (ex) {
            throw ex
        }
    }

    async getPrice(priceId) {
        try {
            return stripe.prices.retrieve(priceId)
        } catch (ex) {
            throw ex
        }
    }

    async listPrices(productId, interval) {
        try {
            const ps = []

            for await (const p of stripe.prices.list({product: productId, recurring: {interval: interval}})) {
                ps.push(p)
            }

            return ps
        } catch (ex) {
            throw ex
        }
    }

    async listPricesWithInterval(productId, interval) {
        try {
            const ps = await this.listPrices(productId)

            if (!!interval) {
                const r = []

                for (const p of ps) {
                    if (p.recurring.interval === interval)
                        r.push(p)
                }

                return r
            }

            return ps
        } catch (ex) {
            throw ex
        }
    }

    async getProduct(productId) {
        try {
            return stripe.products.retrieve(productId)
        } catch (ex) {
            throw ex
        }
    }

    async deleteSubscription(id) {
      try {
        return await stripe.subscriptions.del(
          id
        )
      } catch (ex) {
        throw ex
      }
    }

}
