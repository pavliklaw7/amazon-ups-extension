import {StripeHelper} from "../helpers/stripeHelper.js";

const stripeHelper = new StripeHelper()

export class stripeController {
    constructor() {
    }

    async generateCheckoutPage(body) {
        const {email, planName, interval} = body

        if (!email)
            throw 'Email must not be empty.'

        try {
            const customer = await stripeHelper.createCustomer(email)
            const product = (await stripeHelper.listProductsWithName(planName))[0]
            const price = (await stripeHelper.listPrices(product.id, interval))[0]

            const session = await stripeHelper.createCheckoutSession(price.id, customer.id)
            return {
                success: true,
                sessionId: session.id
            }
        } catch (ex) {
            return {
                success: false,
                message: ex.message
            }
        }
    }

    async checkSubscription(body) {
        try {
            const {email, planName} = body

            const customerId = await stripeHelper.getIdByEmail(email)
            const subscriptions = await stripeHelper.listSubs(customerId)
            /*            const products = []

                        for(const s of subscriptions){
                            const product = await stripeHelper.getProduct(s.plan.product)
                            if(product.name === plan){}
                        }*/

            if (!subscriptions || subscriptions.length === 0)
                return {
                    success: true,
                    active: false
                }

            const products = []
            if (subscriptions.length !== 0) {
                for (let sub of subscriptions) {
                    if (sub.status === 'active') {
                        const {items} = sub
                        for (let item of items.data) {
                            const product = await stripeHelper.getProduct(item.price.product)
                            products.push(product)
                        }
                    }
                }
            }

            if (!!planName) {
                for (let p of products)
                    if (p.name === planName)
                        return {
                            success: true,
                            active: true
                        }
            } else {
                if (subscriptions[0].status === 'active')
                    return {
                        success: true,
                        active: true
                    }
            }

            return {
                success: true,
                active: false
            }
        } catch (ex) {
            return {
                success: false,
                message: ex.message
            }
        }
    }
}
