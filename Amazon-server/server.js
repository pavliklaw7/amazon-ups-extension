import {stripeController} from "./controllers/stripeController.js";
import {createRequire} from 'module';
import {StripeHelper} from "./helpers/stripeHelper.js";

const require = createRequire(import.meta.url)
const path =require('path')
const __dirname = path.resolve()

let express = require('express'),
    app = express(),
    port = 3000

app.use(express.json())


let bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.listen(port, () => {
    console.log(`listening on ${ port }`)
})


let stripeCont = new stripeController();
let stripe = new StripeHelper();

app.post('/stripe/checkSubscription', async (req, res, next) => {
  let result = await stripeCont.checkSubscription(req.body)
  res.json(result)
})

app.post('/stripe/checkoutPage', async (req, res, next) => {
  console.log('Log from 36: ', req.body);
  let result = await stripeCont.generateCheckoutPage(req.body)
  res.json(result)
})

app.post('/stripe/cancelSubscription', async (req, res, next) => {
  let email = await stripe.getIdByEmail(req.body.email);
  let subscriptions = await stripe.listSubs(email);
  await stripe.deleteSubscription(subscriptions[0].id);
  res.json("Deleted!");
})

app.get('/Subscribe', (req, res, next) => {
  res.sendFile(__dirname + `/views/index.html`);
})
