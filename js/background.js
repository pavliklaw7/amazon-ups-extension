const { goToUps, settingConfig, runUpsAutocomplite } = config.keys;

chrome.browserAction.onClicked.addListener(function (tab) {
  console.log('Clicked!');
  chrome.runtime.sendMessage(new ExtensionMessage('check_login', {}));
});

chrome.runtime.onMessage.addListener(async function (message, sender) {
  console.log("Message context: ", message.context);
  switch (message.context) {
    case 'popup_opened':
      chrome.runtime.sendMessage(new ExtensionMessage('check_login', '123'));
      break;

    case goToUps:
      chrome.tabs.create(
        { url: 'https://wwwapps.ups.com/pickup/schedule?loc=en_US' },
        async (tab) => {
          // sendPageMessage(upsAutocomplite, tab.id);
          console.log(tab.title);
        },
      );
      break;

    case config.keys.sheets:
      console.log(message.data);
      $.ajax({
        type: 'POST',
        url: 'https://amazon-ups-extension.glitch.me/uploadShipping',
        data: message.data,
        success: function (response) {
          console.log(response);
        },
      });
      break;

    case OAUTH.key.auth:
      await OAUTH.request.openAuth(message, sender);
      break;

    case OAUTH.key.authCodeReceived:
      await OAUTH.request.saveUser(message, sender);
      let profile = await OAUTH.request.getProfileInfo();
      console.log(profile);
      await $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/stripe/checkoutPage',
        data: profile,
        success: (res) => {
          console.log(res);
          chrome.runtime.sendMessage(new ExtensionMessage('logged_in', profile));
        }
      })
      await $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/stripe/checkSubscription',
        data: profile,
        success: (res) => {
          console.log(res);
          chrome.runtime.sendMessage(new ExtensionMessage('active_status', res));
        }
      });
      break;
    
    case 'unsubscribe':
      let profile2 = await OAUTH.request.getProfileInfo();
      await $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/stripe/cancelSubscription',
        data: profile2.email,
        success: (res) => {
          console.log(res);
        }
      })
      break;

    case 'redirect_to_checkout':
      console.log(message.data.email);
      $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/stripe/checkoutPage',
        data: {
          email: message.data.email,
          planName: 'Test_plan',
          interval: 'month'
        },
        success: function (response) {
          console.log(response);
        },
      });
      break;
  }
});

chrome.runtime.onInstalled.addListener(function () {
  storageSet({ key: settingConfig, value: {} });
});
