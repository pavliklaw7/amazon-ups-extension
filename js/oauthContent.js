async function init(){
    let container = await elementAppear(`button`);
    let code = container.parentElement.parentElement.querySelector("textarea").textContent;
    chrome.runtime.sendMessage(new ExtensionMessage(OAUTH.key.authCodeReceived, {code}));
}
init();
