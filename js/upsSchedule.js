document.addEventListener("DOMContentLoaded", () => {

  /* Gloval Variables */

  const {
    companyName,
    customerName,
    customerPhone,
    earliestPickupTime,
    latestPickupTime,
    specialInstructions,
    pickupPoint,
    numberOfBoxes
  } = config.values;

  upsAutoComplete();

  async function upsAutoComplete() {
    await elementAppear('#chkSrvDomId6');

    const companyNameInput = document.querySelector('#addrMDCompanyId');
    const customerNameInput = document.querySelector('#addrMDCustNameId');
    const resAddressCheckbox = document.querySelector('#chkResiAddr');
    const totalPackagesInput = document.querySelector('#dtotalpkgs');
    const customerPhoneInput = document.querySelector('#addrMDPhoneId');

    const roomIdInput = document.querySelector('#addrMDRoomId');
    const floorId = document.querySelector('#addrMDFloorId');

    const upsGroundCheckbox = document.querySelector('#chkSrvDomId6');

    const earliestPickupTimeCheckbox = document.querySelector('#readyHours');
    const latestPickupTimeCheckbox = document.querySelector('#closeHours');
    const pickupPointSelect = document.querySelector('#pickuppoint');
    const specInstrTextarea = document.querySelector('#spInstrId');

    const nextBtn = document.querySelector('#nextButtonId');

    let saveUps = await storageGet(config.keys.settingConfigUps)
    if(saveUps?.customerNameInput){
      companyNameInput.value = saveUps.customerNameInput;
    }
    else{
      companyNameInput.value = companyName;
    }

    if(saveUps?.contactNameInput){
      customerNameInput.value = saveUps.contactNameInput;
    }
    else{
      customerNameInput.value = customerName;
    }

    if(saveUps?.telephoneInput){
      customerPhoneInput.value = saveUps.telephoneInput;
    }
    else{
      customerPhoneInput.value = customerPhone;
    }

    if(saveUps?.suiteRoomInput){
      roomIdInput.value = saveUps.suiteRoomInput;
    }

    if(saveUps?.floorInput){
      floorId.value = saveUps.floorInput;
    }

    if(saveUps?.residentialAddressCheck != undefined && saveUps?.residentialAddressCheck != null){
      if(resAddressCheckbox.checked){
        if(saveUps?.residentialAddressCheck){

        }
        else{
          resAddressCheckbox.click();
        }
      }
      else{
        if(saveUps?.residentialAddressCheck){
          resAddressCheckbox.click();
        }
        else{
        }
      }
    }
    else{
      resAddressCheckbox.click();
    }
    
    totalPackagesInput.value = numberOfBoxes;
    upsGroundCheckbox.click();

    if (earliestPickupTimeCheckbox.children.length <= 8) {
      earliestPickupTimeCheckbox.value = +earliestPickupTimeCheckbox.children[0].value;
    } else {
      earliestPickupTimeCheckbox.value = earliestPickupTime;
    }

    latestPickupTimeCheckbox.value = latestPickupTime;
    pickupPointSelect.value = pickupPoint;
    
    if(saveUps?.specInstructionTextarea){
      specInstrTextarea.value = saveUps.specInstructionTextarea;
    }
    else{
      specInstrTextarea.value = specialInstructions;
    }

    nextBtn.click();
  };
});
