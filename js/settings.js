document.addEventListener('DOMContentLoaded', async () => {

  /* ------- UPS ------ */

  /* Global Variables */

  const prevSettingConfig = await storageGet(config.keys.settingConfigAmazon);

  console.log('Prev Config', prevSettingConfig);


  const currentConfig = {};

  let upsStep = 1;

  const stepBtns = Array.from(document.querySelectorAll('.nav__item'));

  const nav = document.querySelector('.nav');

  const step1 = document.getElementById('upsStep1');
  const step2 = document.getElementById('upsStep2');
  const step3 = document.getElementById('upsStep3');
  const step4 = document.getElementById('upsStep4');
  const step5 = document.getElementById('upsStep5');

  const nextBtn = document.querySelector('#next');
  const prevBtn = document.querySelector('#prev');
 // remove empty str in textareas

  /* event handlers */

  nextBtn.addEventListener('click', next);

  prevBtn.addEventListener('click', prev);

  function next() {

    const pregressStep = Array.from(document.querySelectorAll('.progress-step'));
  
    if (upsStep === 1) {
      upsStep = 2;
      changeStepBtnWithNext(upsStep);
      step1.classList.remove("is-active");
      step1.classList.add("is-complete");
      step2.classList.add("is-active");

    } else if (upsStep === 2) {
      upsStep = 3;
      changeStepBtnWithNext(upsStep);
      step2.classList.remove("is-active");
      step2.classList.add("is-complete");
      step3.classList.add("is-active");

    } else if (upsStep === 3) {
      upsStep = 4;
      changeStepBtnWithNext(upsStep);
      step3.classList.remove("is-active");
      step3.classList.add("is-complete");
      step4.classList.add("is-active");

    } else if (upsStep === 4) {
      upsStep = 5;
      changeStepBtnWithNext(upsStep);
      step4.classList.remove("is-active");
      step4.classList.add("is-complete");
      step5.classList.add("is-active");
      nextBtn.innerText = 'Save';

    } else if (upsStep === 5) {
      upsStep = 1;
      changeStepBtnWithNext(upsStep);
      step5.classList.remove("is-active");
      step4.classList.remove("is-complete");
      step3.classList.remove("is-complete");
      step2.classList.remove("is-complete");
      step1.classList.remove("is-complete");
      stepBtns.forEach(btn => btn.classList.remove("is-complete", "is-active", "is-current"));
      stepBtns[0].classList.add("is-active");
      step1.classList.add("is-active");
      simpleNotify.notify("The configuration has been saved ", "good", 6000);
      nextBtn.innerText = 'Next';
      saveUpsData();
    }

    if (pregressStep[upsStep - 1].classList.contains('is-complete')) {
      changeStepBtnWithNext(upsStep);
      pregressStep[upsStep - 1].classList.remove('is-active',);
    }
  }

  async function saveUpsData() {
    const customerNameInput = document.querySelector('#customerName').value;
    const contactNameInput = document.querySelector('#contactName').value;
    const suiteRoomInput = document.querySelector('#suiteRoom').value;
    const floorInput = document.querySelector('#floor').value;
    const residentialAddressCheck = document.querySelector('#residentialAddress').checked;
    const telephoneInput = document.querySelector('#telephone').value;
    const extInput = document.querySelector('#ext').value;
    const specInstructionTextarea = document.querySelector('#specInstruction').value;

    const submitUpsOrderCheck = document.querySelector('#submitUpsOrder').checked;

    const arrOfInputs = {
      customerNameInput,
      contactNameInput,
      suiteRoomInput,
      floorInput,
      residentialAddressCheck,
      telephoneInput,
      extInput,
      specInstructionTextarea,
      submitUpsOrderCheck
    };

    await storageSet({key: config.keys.settingConfigUps, value: arrOfInputs});
  }


  function prev() {

    if (upsStep === 1) {
      return;
    }

    displayContent(upsStep - 1);
    changeStepBtnWithPrev(upsStep - 1)

    upsStep -= 1;
  }

  function changeStepBtnWithNext(currentStep) {

    if (!stepBtns[currentStep - 1].classList.contains('is-complete')) {
      stepBtns[currentStep - 1].classList.add('is-active');

      stepBtns[currentStep - 2].classList.remove('is-active');
      stepBtns[currentStep - 2].classList.remove('is-current');

      stepBtns[currentStep - 2].classList.add('is-complete');

    }

    stepBtns[currentStep - 1].classList.add('is-current');

    displayContent(currentStep);
  }

  function changeStepBtnWithPrev(currentStep) {

    stepBtns[currentStep - 1].classList.add('is-current');


   if (stepBtns[currentStep].classList.contains('is-complete')) {
    stepBtns[currentStep].classList.remove('is-current');
    stepBtns[currentStep].classList.remove('is-active');
   }

    stepBtns[currentStep].classList.remove('is-current');


    displayContent(currentStep);
  }

  function changeStepBtnWithTab(currStep) {
    stepBtns.forEach(btn => {
      btn.classList.remove('is-current');
    });

    stepBtns[currStep - 1].classList.add('is-current');

    upsStep = currStep;
  };


  function displayContent(step) {
    const sections = Array.from(document.querySelectorAll('.content.ups-item'));

    sections.forEach(section => section.style.display = 'none');

    sections[step - 1].style.display = 'block';
  }


  Array.from(nav.children).forEach((navBtn, i) => navBtn.onclick = () => {
    displayContent(i + 1);
    changeStepBtnWithTab(i + 1);
  });

  /* quantity */

  $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
  $('.quantity').each(function() {
    var spinner = jQuery(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find('.quantity-up'),
      btnDown = spinner.find('.quantity-down'),
      min = input.attr('min'),
      max = input.attr('max');

    btnUp.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });
  });


  /* Add new Phone number */

  const mobileNumberAddBtn = document.querySelector('#mobileNumberAddBtn');

  mobileNumberAddBtn.onclick = createAddPhoneForm;

  function createAddPhoneForm() {
    if (mobileNumberAddBtn.nextElementSibling === null) {
      const addPhoneForm = document.createElement('form');
      addPhoneForm.setAttribute('id', 'addPhoneForm');
      addPhoneForm.setAttribute('action', '#');
      addPhoneForm.setAttribute('method', 'POST');
      addPhoneForm.setAttribute('class', 'add-new');

      const addPhoneInput = document.createElement('input');
      addPhoneInput.setAttribute('id', 'addPhoneInput');
      addPhoneInput.setAttribute('type', 'text');
      addPhoneInput.setAttribute('placeholder', 'Enter new Number');
      addPhoneInput.setAttribute('class', 'add-new__input');

      const addPhoneBtn = document.createElement('button');
      addPhoneBtn.setAttribute('id', 'addPhoneBtn');
      addPhoneBtn.setAttribute('type', 'submit');
      addPhoneBtn.setAttribute('class', 'add-new__btn');
      addPhoneBtn.innerText = '+';

      addPhoneForm.insertAdjacentElement('afterbegin', addPhoneInput);
      addPhoneForm.insertAdjacentElement('beforeend', addPhoneBtn);

      mobileNumberAddBtn.after(addPhoneForm);

      addPhoneBtn.onclick = addNewPhone
    } 
  };

  
  function addNewPhone(event) {
    event.preventDefault();

    const newPhone = document.querySelector('#addPhoneInput');

    const option = document.createElement('option');
    option.innerText = newPhone.value;
    option.value = newPhone.value;
    option.selected = true;

    document.querySelector('#phoneNumber').insertAdjacentElement('afterbegin', option);

    newPhone.value = '';
  };


  /* -------- Aside Nav --------- */

  const asideBtns = document.querySelectorAll('.aside__item');

  function clearBtnStyle() {
    asideBtns.forEach(btn => btn.classList.remove('is-active'));
    asideBtns.forEach(btn => Array.from(document.querySelectorAll(`.${btn.name}`)).forEach(targetElement => targetElement.style.display = 'none'));
  }

  asideBtns.forEach(btn => btn.addEventListener('click', (event) => {

    if (!event.target.classList.contains('is-active')) {
      clearBtnStyle();      
      event.target.classList.add('is-active');
      Array.from(document.querySelectorAll(`.${event.target.name}`)).forEach(targetElement => targetElement.style.display = 'block');
    }
  }));

   /* ------- Amazon ------ */

  let amazonStep = 1;

  const amazonStepBtns = Array.from(document.querySelectorAll('.amazon-item .nav__item'));

  const amazonNav = document.querySelector('.nav.amazon-item');

  const amazonStep1 = document.getElementById('amazonStep1');
  const amazonStep2 = document.getElementById('amazonStep2');
  const amazonStep3 = document.getElementById('amazonStep3');
  const amazonStep4 = document.getElementById('amazonStep4');
  const amazonStep5 = document.getElementById('amazonStep5');

  const amazonSaveBtn = document.querySelector('.amazon #save');

   // remove empty str in textareas

  /* event handlers */

  amazonSaveBtn.onclick = saveAmazonData;


  async function saveAmazonData() {
    const amazonInputs = Array.from(document.querySelectorAll('.amazon input'));

    amazonInputs.forEach(input => {
      if (input.value != 'on' && input.value != '' ) {
        currentConfig[input.id] = input.value
      } else if (input.checked) {
        currentConfig[input.id] = true;
      } else {
        currentConfig[input.id] = false;
      }
    });

    const amazonSelect = Array.from(document.querySelectorAll('.amazon select'));

    amazonSelect.forEach(select => currentConfig[select.id] = select.value)

    simpleNotify.notify("The configuration has been saved ", "good", 6000);

    console.log(currentConfig);
    await storageSet({key: config.keys.settingConfigAmazon, value: currentConfig});
  }

  $('#shipingDate').datepicker();
   $('#shipingDate').focus(function() {
     $(this).datepicker("show");
     setTimeout(function() {
       $('#shipingDate').datepicker("hide");
       $('#shipingDate').blur();
     }, 2000)
   })    
});

init()

async function init(){
  loadsaved()
  let amazonSaved = await storageGet(config.keys.settingConfigAmazon);
  if(amazonSaved)
    document.querySelector("#paperType").value = amazonSaved.paperType
    document.querySelector("#avgPrUnit").value = amazonSaved.avgPrUnit
    document.querySelector('#redirectUps').checked = amazonSaved.redirectUps
    document.querySelector("#maxBox").value = amazonSaved.maxBox
}

async function loadsaved(){
  let saveUps = await storageGet(config.keys.settingConfigUps)
  if(saveUps){
    document.querySelector('#customerName').value = saveUps.customerNameInput
    document.querySelector('#contactName').value = saveUps.contactNameInput
    document.querySelector('#suiteRoom').value = saveUps.suiteRoomInput
    document.querySelector('#floor').value = saveUps.floorInput
    document.querySelector('#residentialAddress').checked = saveUps.residentialAddressCheck
    document.querySelector('#telephone').value = saveUps.telephoneInput
    document.querySelector('#ext').value = saveUps.extInput

    document.querySelector('#specInstruction').value = saveUps.specInstructionTextarea
    document.querySelector('#submitUpsOrder').checked = saveUps.submitUpsOrderCheck
  }
}