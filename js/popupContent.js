let loginBlock = document.querySelector('.login');
let subscribeBlock = document.querySelector('.subscribe');
let subscribeStatusBlock = document.querySelector('.subscribe-status');
let footerEmail = document.querySelector('.footer__email');
let exitButton = document.querySelector('.footer__exit-button');
let settingsButton = document.querySelector('.header__settings-icon');
let nameLine = document.querySelector('.subscribe__head');
let checkoutButton = document.querySelector('.subscribe__button');

chrome.runtime.sendMessage(new ExtensionMessage('popup_opened'));

settingsButton.addEventListener('click', () => {
  chrome.tabs.create({ url: chrome.runtime.getURL('./settings.html') }, () => {
    console.log(tab);
  });
})

async function authRequest() {
  let result = await requestBackground(new ExtensionMessage(OAUTH.key.auth));
}

let googleButton = document.querySelector('.login__button');
googleButton.addEventListener('click', () => {
  authRequest();
})

async function checkLogin() {
  let loggedEmail = await storageGet('logged_email');
  console.log(loggedEmail);
  if(loggedEmail != undefined) {
    loginBlock.style.display = 'none';
    subscribeBlock.style.display = 'block';
    settingsButton.style.display = 'block';
    nameLine.textContent = `Hi, ${loggedEmail.given_name}!`
    footerEmail.textContent = loggedEmail.email;
    exitButton.style.display = 'block';
  }
}

checkoutButton.addEventListener('click', async (e) => {
  e.preventDefault()
  let userInfo = await storageGet('logged_email');
  chrome.runtime.sendMessage(new ExtensionMessage('redirect_to_checkout', userInfo));
  chrome.tabs.create({ url: `http://localhost:3000/Subscribe?email=${userInfo.email}&plan=Test_plan`}, () => {
    console.log(tab);
  });
})

chrome.runtime.onMessage.addListener(async function(message) {
  console.log(message.context, message.data);
  switch (message.context) {
    case 'logged_in':
      await storageSet({key: 'logged_email', value: message.data});
      await checkLogin();
      break;
    
    case 'check_login':
      await checkLogin();
      break;
  }
});


