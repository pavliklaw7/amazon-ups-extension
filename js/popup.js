const {
  runUpsAutocomplite,
  runAmazonAutocomplite,
} = config.keys;

init();

async function init() {

  const popupBtn = document.querySelector('.button');

  popupBtn.addEventListener('click', async () => {
    sendPageMessage(runAmazonAutocomplite);
  });
}

