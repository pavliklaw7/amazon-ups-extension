var config = {
  keys: {
    runUpsAutocomplite: 'runUpsAutocomplite',
    runAmazonAutocomplite: 'runAmazonAutocomplite',
    upsAutocomplite: 'upsAutocomplite',
    amazonAutocomplite: 'amazonAutocomplite',
    boxesCount: 'boxesCount', // for storageSet
    goToUps: 'goToUps',
    settingConfigAmazon: 'settingConfigAmazon',
    settingConfigUps: 'settingConfigUps',
    sheets: 'sheets',
  },
  values: {
    companyName: 'Take 2 Company',
    customerName: 'Jay Price',
    customerPhone: '3166175310',
    numberOfBoxes: 2,
    //boxWeight: 51,
    //boxDimensions: {
    //length: 26,
    //width: 14,
    //height: 27
    //},
    paperType: 'Thermal',
    shipDate: new Date().getDate(),
    earliestPickupTime: 10,
    latestPickupTime: 7,
    specialInstructions: 'Enter thru Side Garage Door. Packages marked "UPS Pickup"',
    pickupPoint: 'Side Door',
    sizeWarningTitle: 'One or more boxes in your shipment exceeded the maximum size requirements.',
    sizeWarningText: `- Boxes containing multiple standard-size items must not exceed 25.00 inches on any side. A box may exceed the 25.00 inch limit if it contains oversize units that measure longer than 25.00 inches. 
        - Boxes that are excessively large relative to the oversize units may be subject to restriction of shipping privileges, additional fees, or refusal at a fulfillment center.`,
    weightWarningTitle:
      'One or more boxes in your shipment exceeded the maximum weight requirements.',
    weightWarningText: `- Boxes must not exceed the standard weight limit of 50.00 lb, unless they contain one
        single oversized item that exceeds 50.00 lb.
            - For a single oversized item that exceeds 50.00 lb, attach a label that clearly
            indicates Team Lift on the top and sides of the box.
            - For a single oversized item that exceeds 100.00 lb, attach a label that clearly
            indicates Mechanical Lift on the top and sides of the box.
        -Boxes containing jewelry or watches must not exceed 40.00 lb.&quot;`,
    avaragePrice: 0.44,
    // suitRoom: 'suit/room',
    // floor: 'floor',
  },
};
