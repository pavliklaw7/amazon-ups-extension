document.addEventListener('DOMContentLoaded', async () => {
  /* Gloval Variables */

  const settingConfig = await storageGet('saveAmazonData');

  console.log(settingConfig);

  const { runAmazonAutocomplite, amazonAutocomplite, boxesCount, goToUps } = config.keys;

  const {
    paperType,
    shipDate,
    numberOfBoxes,
    boxWeight,
    boxDimensions,
    sizeWarningTitle,
    sizeWarningText,
    weightWarningTitle,
    weightWarningText,
    avaragePrice,
  } = config.values;

  chrome.runtime.onMessage.addListener((message) => {
    switch (message) {
      case runAmazonAutocomplite:
        amazonAutoComplete();
        console.log(message);
    }
  });

  await elementAppear('#fba-inbound-shipment-workflow-prepare-type-estimate');

  amazonAutoComplete();

  async function amazonAutoComplete() {
    let amazonSaved = await storageGet(config.keys.settingConfigAmazon);

    const boxesCountInput = document.querySelector(
      '.fba-core-input.fba-core-input-number.box-input.number',
    );
    const setBoxesCountBtn = document.querySelector('button[name="Set number of boxes"]');
    const boxWeightInput = document.querySelector(
      '.fba-core-input.fba-core-input-number.box-input.validate-positive-float[name="weight"]',
    );
    const boxLengthInput = document.querySelector(
      '.fba-core-input.fba-core-input-number.box-input.validate-positive-float[name="length"]',
    );
    const boxWidthInput = document.querySelector(
      '.fba-core-input.fba-core-input-number.box-input.validate-positive-float[name="width"]',
    );
    const boxHeightInput = document.querySelector(
      '.fba-core-input.fba-core-input-number.box-input.validate-positive-float[name="height"]',
    );
    const shippingChargeCalculateBtn = document.querySelector(
      '#fba-inbound-shipment-workflow-prepare-type-estimate',
    );

    await elementAppear('#fba-inbound-shipment-workflow-prepare-type-labels-button');
    const completeShipmentBtn = document.querySelector(
      '#fba-inbound-shipment-workflow-prepare-type-actions-continue',
    );

    // boxesCountInput.value = numberOfBoxes;
    // setBoxesCountBtn.click();
    // boxWeightInput.value = boxWeight;
    // boxLengthInput.value = boxDimensions.length;
    // boxWidthInput.value = boxDimensions.width;
    // boxHeightInput.value = boxDimensions.height;
    await elementAppear(
      '.fba-core-input.fba-core-input-number.box-input.validate-positive-float',
    )

    const shipmentPackingInputs = Array.from(
      document.querySelectorAll(
        '.fba-core-input.fba-core-input-number.box-input.validate-positive-float',
      ),
    );
    const weightInputs = shipmentPackingInputs.filter(
      (input) => shipmentPackingInputs.indexOf(input) % 4 === 0,
    );
    const weightInputsValues = weightInputs.map((weight) => +weight.value);

    for (let i = 0; i < weightInputsValues.length; i++) {
      if (weightInputsValues[i] > 50) {
        toggleWarningStyle(weightInputs[i]);
        await displayBoxWarning(weightWarningTitle, weightWarningText);
      }
    }

    const sizeInputs = shipmentPackingInputs.filter(
      (input) => shipmentPackingInputs.indexOf(input) % 4 !== 0,
    );
    const sizeInputsValues = sizeInputs.map((size) => +size.value);

    if(amazonSaved && amazonSaved?.maxBox && (+amazonSaved?.maxBox !== 0)){
      for (let i = 0; i < sizeInputsValues.length; i++) {
        if (sizeInputsValues[i] > +amazonSaved.maxBox) {
          sizeInputs.forEach((input) => (input.value > +amazonSaved.maxBox ? toggleWarningStyle(input) : null));
          await displayBoxWarning(sizeWarningTitle, sizeWarningText);
          break;
        }
      }
    }

    if (shippingChargeCalculateBtn) {
      shippingChargeCalculateBtn.click();

      await elementAppear('#fba-inbound-shipment-workflow-prepare-type-accept');

      if(amazonSaved && amazonSaved?.avgPrUnit && (+amazonSaved?.avgPrUnit !== 0))
        await displayCostWarning();

      const shippinChargeAgreeCheckbox = document.querySelector(
        '#fba-inbound-shipment-workflow-prepare-type-agree',
      );
      const shippinChargeAcceptBtn = document.querySelector(
        '#fba-inbound-shipment-workflow-prepare-type-accept',
      );
      shippinChargeAgreeCheckbox.click();
      shippinChargeAcceptBtn.click();
    } else {
      await elementAppear('#fba-inbound-shipment-workflow-prepare-type-accept');

      const shippinChargeAgreeCheckbox = document.querySelector(
        '#fba-inbound-shipment-workflow-prepare-type-agree',
      );
      const shippinChargeAcceptBtn = document.querySelector(
        '#fba-inbound-shipment-workflow-prepare-type-accept',
      );
      shippinChargeAgreeCheckbox.click();
      shippinChargeAcceptBtn.click();
    }

    await elementAppear(
      '#fba-inbound-shipment-workflow-template-prepare-type-print-label-group-input',
    );

    const paperTypeSelect = document.querySelector(
      '#fba-inbound-shipment-workflow-template-prepare-type-print-label-group-input',
    );
    // const shipDateInput = document.querySelector('#fba-inbound-shipment-workflow-ship-date');
    const printBoxBtn = document.querySelector(
      '#fba-inbound-shipment-workflow-prepare-type-labels-button',
    );

    await elementAppear('#fba-inbound-shipment-workflow-ship-date');
    simulate(document.querySelector('#fba-inbound-shipment-workflow-ship-date'), 'focus');
    simulate(document.querySelector('#fba-inbound-shipment-workflow-ship-date'), 'input');
    simulate(document.querySelector('#fba-inbound-shipment-workflow-ship-date'), 'change');

    await elementAppear('#ui-datepicker-div');

    const datepickerDiv = document.querySelector('#ui-datepicker-div');

    const dates = Array.from(datepickerDiv.querySelectorAll('.ui-state-default'));

    if(amazonSaved?.paperType)
      paperTypeSelect.value = amazonSaved.paperType;
    else
      paperTypeSelect.value = paperType;

    dates[shipDate - 1].click();

    await sleep(3000);

    // Parse content to object A.
    let unitCount = +document.querySelector(
      '#fba-core-workflow-meta-data-shipment-contents-total-quantity',
    ).innerText;
    let shippingCost = document.querySelector('td.number strong.important');
    let shippingCostValue = +shippingCost.innerText.replace('$', '');
    let costPerUnit = shippingCostValue / unitCount;
    let costPerUnitParse = +costPerUnit.toFixed(3);
    let shippingDestinationIndex = document.querySelector(
      '#fba-core-view-meta-data-ship-to > dl > dd',
    ).innerText;
    let shippingWeight = +document.querySelector(
      '#fba-inbound-shipment-workflow-prepare-type-estimate-data-total-weight',
    ).innerText;
    let billableWeight = document.querySelector(
      '#fba-inbound-shipment-workflow-prepare-type-spd-estimate-section .fba-core-input table td:nth-child(4)',
    ).innerText;
    let billableWeightParse = parseInt(billableWeight.replace(/[^\d]/g, ''));
    let perLbShippingCost = shippingCostValue / shippingWeight;
    let perLbShippingCostParse = +perLbShippingCost.toFixed(3);
    const sheetsObj = {
      costPerUnitParse,
      perLbShippingCostParse,
      shippingDestinationIndex,
      shippingWeight,
      billableWeightParse,
      unitCount,
      shippingCostValue,
    };
    requestBackground(new ExtensionMessage(config.keys.sheets, sheetsObj));
    //

    printBoxBtn.click();

    completeShipmentBtn.click();

    await storageSet({key: boxesCount, value: numberOfBoxes});

    if(amazonSaved?.redirectUps)
      requestBackground(new ExtensionMessage(goToUps));

    await elementAppear('button[name="mark-as-shipped"]');
    const markAsShippedBtn = document.querySelector('button[name="mark-as-shipped"]'); // final btn
    markAsShippedBtn.click();

    async function displayBoxWarning(title, text) {
      const modalWrapper = document.querySelector(
        '#fba-inbound-shipment-workflow-view-prepare-type-spd-packages',
      );

      return new Promise((resolve, reject) => {
        modalWrapper.insertAdjacentHTML(
          'beforeend',
          `
          <div class="modal" id="modal">
            <h2 class="modal__title">${title}</h2>
            <div class="modal__content">${text.replace(/(25\.00)/gi, amazonSaved.maxBox)}</div>
            <div class="modal__actions">
              <button id="acceptWarning" class="modal__button">
                <span>Accept</span>
              </button>
              <button id="stopAutocomplite" class="modal__button">
                <span>Stop Autocomplete</span>
              </button>
            </div>
          </div> `,
        );

        scrollToElement(modalWrapper);

        const modal = document.querySelector('.modal');
        const acceptBtn = document.querySelector('#acceptWarning');
        const stopBtn = document.querySelector('#stopAutocomplite');

        acceptBtn.addEventListener('click', () => {
          toggleWarningStyle();
          modal.remove();

          const wrapper = document.querySelector(
            '#fba-inbound-shipment-workflow-prepare-type-boxes-info-table .actions-footer td',
          );

          wrapper.insertAdjacentHTML(
            'beforeend',
            `<button id="continueAutocomplite" class="modal__button">
              <span>Continue Autocomplete</span>
            </button>`,
          );

          const continueBtn = document.querySelector('#continueAutocomplite');

          continueBtn.addEventListener('click', () => {
            resolve();
          });
        });

        stopBtn.addEventListener('click', () => {
          toggleWarningStyle();
          modal.remove();
          reject();
        });
      });
    }

    async function displayCostWarning() {
      return new Promise((resolve, reject) => {
        const unitCount = +document.querySelector(
          '#fba-core-workflow-meta-data-shipment-contents-total-quantity',
        ).innerText;
        const shippingCost = document.querySelector('td.number strong.important');
        const shippingCostValue = +shippingCost.innerText.replace('$', '');
        const costPerUnit = shippingCostValue / unitCount;
        const text = `Your avg. price is $${amazonSaved.avgPrUnit}. This inbound shipment is $${costPerUnit.toFixed(
          2,
        )} per unit. Are sure you want to create this shipment?`;

        const modalWrapper = document.querySelector(
          '#fba-inbound-shipment-workflow-prepare-type-spd-estimate-section > div',
        );

        toggleWarningStyle(shippingCost);

        if (costPerUnit > amazonSaved.avgPrUnit) {
          modalWrapper.insertAdjacentHTML(
            'beforeend',
            `
            <div class="modal top20 m0" id="modal">
              <h2 class="modal__title">Warning!</h2>
              <div class="modal__content mt10">${text}</div>
              <div class="modal__actions p4">
                <button id="continueAutocomplite" class="modal__button">
                  <span>Continue</span>
                </button>
                <button id="stopAutocomplite" class="modal__button">
                  <span>Stop Autocomplete</span>
                </button>
              </div>
            </div> `,
          );
          scrollToElement(modalWrapper);
        } else {
          resolve();
        }

        const modal = document.querySelector('.modal');
        const continueBtn = document.querySelector('#continueAutocomplite');
        const stopBtn = document.querySelector('#stopAutocomplite');

        continueBtn.addEventListener('click', () => {
          toggleWarningStyle(shippingCost);
          modal.classList.toggle('off');
          resolve();
        });

        stopBtn.addEventListener('click', () => {
          toggleWarningStyle(shippingCost);
          modal.classList.toggle('off');
          reject();
        });
      });
    }

    function toggleWarningStyle(element) {
      const shippingCost = document.querySelector('td.number strong.important');

      if (element === shippingCost) {
        shippingCost.style.outline === '2px solid #f90'
          ? shippingCost.style.removeProperty('outline')
          : (shippingCost.style.outline = '2px solid #f90');
      }

      if (element) {
        element.style.borderColor = '#f90';
      } else {
        shipmentPackingInputs.forEach((input) => input.style.removeProperty('border-color'));
      }
    }
    //}
  }

  function scrollToElement(element) {
    element.scrollIntoView({ block: 'center', behavior: 'smooth' });
  }
});
