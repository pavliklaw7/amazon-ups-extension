document.addEventListener("DOMContentLoaded", () => {

  runUpsPaymentAutocomplite();

  async function runUpsPaymentAutocomplite() {

    await elementAppear('#billToCCId');

    const paymentInfo = document.querySelector('#billToCCId');

    paymentInfo.click();

    await elementAppear('#cpcWidget_paymentMethodSelect');

    const paymentMethod = document.querySelector('#cpcWidget_paymentMethodSelect');

    paymentMethod.value = Array.from(paymentMethod.children)[1].value; // regexp

    let saveUps = await storageGet(config.keys.settingConfigUps)
    if(saveUps?.submitUpsOrderCheck){
      //document.querySelector(".ups-form_ctaGroup #SUBMIT_BUTTON").click()
    }
  };
});
