let subscribeButton = document.querySelector('.subscribe-button');

chrome.runtime.onMessage.addListener(function(message) {
  console.log(message.context, message.data);
  switch (message.data.active) {
    case true:
      subscribeButton.style.display = "block";
      subscribeButton.innerText = "Unsubscribe!"
      break;

    case false:
      subscribeButton.style.display = "block";
      subscribeButton.innerText = "Subscribe!"
    }
})

let subsButton = document.querySelector('.subscribe-button');
subsButton.addEventListener('click', async () => {
  await requestBackground(new ExtensionMessage('unsubscribe'));
})

